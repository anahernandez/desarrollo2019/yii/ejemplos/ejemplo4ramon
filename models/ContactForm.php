<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $nombre;
    public $email;
    public $tema;
    public $descripcion;
 


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['nombre', 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
             ['nombre', 'string', 'max' => 20],
            [['descripcion','tema'],'safe'],
            // Carga masiva de datos
            //[['nombre','email'],'safe']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Escribe tu nombre',
            'email'=> 'Escribe el correo electrónico de contacto',
            'tema'=> 'Sobre qué producto quieres información',
            'descripción' => 'Indica un poco más que información necesitas',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject($this->tema)
                ->setTextBody($this->descripcion)
                ->send();

            return true;
        }
        return false;
    }
}
