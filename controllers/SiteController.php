<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud() {
        return $this->render("gestion");
    }
    
    
    
    public function actionConsulta0() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista')
                //Esto es para decirle el número total de registros
                //Si fuesen varios resultados queryAll()
                //Como solo es uno queryScalar
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select * from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>[],
            "titulo"=> "Consulta 0 sin Active Record",
            "enunciado"=>"Listar todos los registros de los ciclistas",
            "sql"=>"select * from ciclista",
        ]);
    }
    
    public function actionConsulta0a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>[],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Listar todos los campos de los ciclistas",
            "sql"=>"select * from ciclista",
        ]);
    }
    
 
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista')
                //Si fuesen varios resultados queryAll()
                //Como solo es uno queryScalar
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 sin Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
     public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach' or nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"G_avg(edad)  (σ_(nomequipo='Banesto')(ciclista)",
        ]);
    }
    
    
    
     public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                //renombro dorsal alias dorsales
                ->select(['dorsales'=>'dorsal'])
                //where(['OR',["<","edad",25],[">","edad",30]),
                ->where(['between','edad',25,30]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            //si lo pongo asi no encuentra dorsales la tengo que declarar en el modelo
            "campos"=>['dorsales'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"Select(dorsal) from ciclista where edad between 25 and 30",
        ]);
    }
    
    
    public function actionConsulta4() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(dorsal) from ciclista where edad between 25 and 30')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad between 25 and 30',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad between 25-30",
        ]);
    }
    
    public function actionConsulta5a() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select('dorsal,nomequipo')
                ->where(['and',['=','nomequipo','Banesto'],['between','edad',28,32]]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            //así nos evitamos el join
            "campos"=>['dorsal','nomequipo0.director'],
            "titulo"=> "Consulta 5a con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas y el nombre del director del equipo cuya edad sea menor que 28 o mayor que 32 y pertenezcan a Banesto",
            "sql"=>"Select(dorsal) from ciclista where edad between 28 and 32 and nomequipo=Banesto",
        ]);
    }
    
    
    public function actionConsulta5() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(dorsal) from ciclista where edad between 28 and 32 y nomequipo=Banesto')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad between 25 and 30 and nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad between 25-30",
        ]);
    }
    
    /*
     * Alguien me pide información
     */
    
    public function actionEnviar(){
        
        Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['informacion'])
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo(['cliente@gmail.com' => 'Andrés López'])
                ->setSubject('Pedir información sobre algo')
                ->setTextBody('quiero más información sobre bolis')
                ->send();

    }
    
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->contact(Yii::$app->params['informacion']);
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
