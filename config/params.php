<?php
//Aquí pongo los emails de los que van a recibir cada tipo de email
return [
    'informacion' => 'ana.hernpell@gmail.com',
    'contacto' => 'ana.hernpell@gmail.com',
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'responder'=> 'empresa@alpe.es',
];
